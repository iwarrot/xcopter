#ifndef SETTING_H
#define SETTING_H

#include <QDialog>
#include <QString>


namespace Ui {
class Setting;
}

class Setting : public QDialog
{
    Q_OBJECT
    
public:
    explicit Setting(QWidget *parent = 0);
    ~Setting();

    //WIFI or Serial
    bool isWIFI;
    //Map ON or OFF
    bool isMapON;

    QString serialPort;
    QString ipAddress;

private slots:
    void slot_OK();

private:
    Ui::Setting *ui;

};

#endif // SETTING_H
