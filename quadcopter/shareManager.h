#ifndef SHARECONTENT_H
#define SHARECONTENT_H

#include <QtCore>

#include "tcpClient.h"
#include "quadcopter.h"
#include "serialSlave.h"
#include "bufferHandle.h"
#include <QSemaphore>
#include <QMutex>


class ShareManager
{
public:
    ShareManager();

    ~ShareManager();

//取消字节对齐
#pragma pack(1)
    typedef struct RC
    {
        quint16 CH1;
        quint16 CH2;
        quint16 CH3;
        quint16 CH4;
        quint16 CH5;
        quint16 CH6;
        quint16 CH7;
    }RC_t;

    typedef struct Angle
    {
        float YAW;
        float ROL;
        float PIT;
    }Angle_t;

    typedef struct GPS
    {
        float longtitude;
        float latitude;
        float elevation;
        float speed;
        float homeLongtitude;
        float homeLatitude;//钉子
        float homeDistance;
        qint8 satelNum;
        char date[6];
        char time[6];
    }GPS_t;

    typedef struct Motor
    {
        qint16 motor1;
        qint16 motor2;
        qint16 motor3;
        qint16 motor4;
    }Motor_t;
#pragma pack()

    //数据缓存区
    QByteArray rxBuffer;

    //GPS
    GPS_t GPS_values;
    //遥控
    RC_t RC_Values;
    //姿态
    Angle_t AHRS_values;
    //电机
    Motor_t MOTOR_values;

    //遥控互斥锁
    QMutex lockRC;
    //姿态互斥锁
    QMutex lockAHRS;
    //GPS互斥锁
    QMutex lockGPS;
    //电机互斥锁
    QMutex lockMOTOR;

    //主线程
    Quadcopter mainThread;
    //串口子线程
    SerialSlave serialThread;
    //TCP子线程
    TcpClient tcpThread;
    //处理线程
    BufferHandle handleThread;

    //初始化共享指针
    void InitSharePointer();

private slots:

};

#endif // SHARECONTENT_H
