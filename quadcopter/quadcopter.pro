#-------------------------------------------------
#
# Project created by QtCreator 2014-09-18T14:41:10
#
#-------------------------------------------------

QT       += core gui network webkitwidgets

CONFIG   += qwt

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = quadcopter
TEMPLATE = app


SOURCES += main.cpp\
    quadcopter.cpp \
    speedo.cpp \
    indicator.cpp \
    setting.cpp \
    setpid.cpp \
    about.cpp \
    qextserialport/win_qextserialport.cpp \
    qextserialport/qextserialbase.cpp \
    serialSlave.cpp \
    tcpClient.cpp \
    shareManager.cpp \
    bufferHandle.cpp

HEADERS  += quadcopter.h \
    speedo.h \
    compass.h \
    indicator.h \
    setting.h \
    setpid.h \
    about.h \
    qextserialport/win_qextserialport.h \
    qextserialport/qextserialbase.h \
    serialSlave.h \
    tcpClient.h \
    shareManager.h \
    bufferHandle.h

FORMS    += quadcopter.ui \
    setting.ui \
    setpid.ui \
    about.ui

RC_FILE = icon.rc
