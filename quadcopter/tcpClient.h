#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QTcpSocket>
#include <QThread>
#include <QString>
#include <QByteArray>


class ShareManager;

class TcpClient : public QObject
{
    Q_OBJECT
public:
    explicit TcpClient(QObject *parent = 0);
    ~TcpClient();
    //socket
    QTcpSocket *socket;
    //子线程
    QThread workThread;
    //共享指针
    ShareManager *sharePointer;

    //连接服务器
    bool connectServer(QString ipAddr = "192.168.1.123", int port = 10086);

private:
    QByteArray TCP_buffer;

    //缓冲区处理
    void socketBufferHandle(QByteArray &buf);

    inline void RCHandle(const QByteArray &buf);

    inline void AHRSHandle(const QByteArray &buf);

    inline void GPSHandle(const QByteArray &buf);

    inline void MOTORHandle(const QByteArray &buf);

public slots:
    void slot_readTcpData();

    void slot_disconnect();
};

#endif // TCPCLIENT_H
