#ifndef QUADCOPTER_H
#define QUADCOPTER_H
#include <QWidget>

#include "speedo.h"
//#include <QFrame>
#include <qwt_compass.h>
//#include <qwt_compass_rose.h>
#include <qwt_dial_needle.h>
#include "indicator.h"

//my ui
#include <QTimer>

#include "setting.h"
#include "setpid.h"
#include "about.h"


#include <QFile>
#include <QTextStream>
#include <QIODevice>
#include <QMainWindow>
#include <QWebDatabase>
#include <QWebSecurityOrigin>
#include <QWebFrame>
#include <QWebView>
#include <QProcess>

//声明外部类
class ShareManager;

namespace Ui {
class Quadcopter;
}

class Quadcopter : public QWidget
{
    Q_OBJECT
public:
    explicit Quadcopter(QWidget *parent = 0);
    ~Quadcopter();

    //共享指针
    ShareManager *sharePointer;

private:
    Ui::Quadcopter *ui;

    QWebView* m_view;

    SpeedoMeter *speedo;
    QwtCompass  *compass;
    AttitudeIndicator *indicator;

    double speedVal;
    //ui
    SetPID  *pid;
    Setting *setting;
    About   *about;

    //外部exe
    QProcess *camera;

    //定时器刷新各值
    int timer_tick;
    int tick_GPS;
    int tick_map;
    QTimer rc_timer;

    //刷新姿态
    inline void refreshAHRS();
    //刷新遥控
    inline void refreshRC();
    //刷新GPS
    inline void refreshGPS();
    //刷新Map
    inline void refreshMAP();
    //刷新电机
    inline void refreshMOTOR();


public slots:


private slots:
    void on_pushButton_setting_clicked();
    void on_pushButton_setpid_clicked();
    void on_pushButton_about_clicked();
    void on_pushButton_map_clicked();

    void slot_startup();

    void slot_update();

    void slot_videoTransfer();

    void slot_calibration();
};

#endif // QUADCOPTER_H
