#include "shareManager.h"

ShareManager::ShareManager()
{
    InitSharePointer();

    mainThread.show();
}

ShareManager::~ShareManager()
{
    qDebug()<<"管理线程析构";
    //自动回收子线程
    if(serialThread.workThread.isRunning())
    {
        qDebug()<<"管理线程析构serial thread exit";
        serialThread.workThread.exit();
        serialThread.workThread.wait(1000);
    }

//    if(tcpThread.workThread.isRunning())
//    {
//        tcpThread.workThread.exit();
//        tcpThread.workThread.wait(1000);
//    }

    if(handleThread.isRunning())
    {
        qDebug()<<"管理线程析构handle thread exit";
        handleThread.terminate();
//        handleThread.wait(1000);
    }
}

void ShareManager::InitSharePointer()
{
    mainThread.sharePointer = this;
    tcpThread.sharePointer = this;
    serialThread.sharePointer = this;
    handleThread.sharePointer = this;
}
