#include "setting.h"
#include "ui_setting.h"

#include <QDebug>

Setting::Setting(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Setting)
{
    ui->setupUi(this);

    serialPort = "COM1";

    ipAddress = "192.168.1.123";

    isWIFI = false;

    isMapON = false;

    connect(ui->pushButton_ok, SIGNAL(clicked()), this, SLOT(slot_OK()));

}

Setting::~Setting()
{
    qDebug()<<"setting析构";
    delete ui;
}

void Setting::slot_OK()
{
    if(ui->tabWidget->currentIndex() == 1)//0 串口，1 wifi
    {isWIFI = true;}
    else
    {isWIFI = false;}

    serialPort = ui->comboBox_port->currentText();

    ipAddress = ui->lineEdit->text();

    close();
}


//void Setting::on_pushButton_ok_clicked()
//{
//    QString port = ui->comboBox_port->currentText();
//    QString baud = ui->comboBox_baud->currentText();

//    emit sendSerialPort(port, baud);
//}
