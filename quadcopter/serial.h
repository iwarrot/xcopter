#ifndef SERIAL_H
#define SERIAL_H

#include <QWidget>
#include <QByteArray>

//serial
//#include "qextserialport/qextserialbase.h"
#include "qextserialport/win_qextserialport.h"


namespace Ui {
class Serial;
}

class Serial : public QWidget
{
    Q_OBJECT
    
public:
    explicit Serial(QWidget *parent = 0);
    ~Serial();

    void display(QString);
    bool openSerial();
    void closeSerial();

private:
    Ui::Serial *ui;
    enum Status
    {
        DATA_START, //0
        DATA_HEAD1,
        DATA_HEAD2,
        DATA_FUNC,
        DATA_LENGTH
    };

    QByteArray dataRecBuf;
    uchar recDataBuf[32];
    int byteCount;
    int func;
    int status;
    int rxState;
    int DATA_LEN_MAX;
    uchar dataBuffer[32];

    bool dsp_flag;
    Win_QextSerialPort *SerialPort;
    QString portName;
    BaudRateType baudRate;

    void bufferHandle(const QByteArray &buf);
    void frameHandle(uchar *buf);
    void closeEvent(QCloseEvent *);

public slots:
    void getSerialPortSlot(QString, QString);

private slots:
    void readSerialSlot();
    void on_pushButton_displayON_clicked();
    void on_pushButton_displayOFF_clicked();
    void on_pushButton_clear_clicked();
};

#endif // SERIAL_H
