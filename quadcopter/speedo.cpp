#include <qpainter.h>
#include <qwt_dial_needle.h>
#include <qwt_round_scale_draw.h>
#include "speedo.h"

SpeedoMeter::SpeedoMeter( QWidget *parent ):
    QwtDial( parent )
//    d_label( "Throttle" )
{

//    setPalette( colorTheme( QColor( Qt::darkGray ).dark( 70 ) ) );
    QwtRoundScaleDraw *scaleDraw = new QwtRoundScaleDraw();
    scaleDraw->setSpacing( 5 );
    scaleDraw->enableComponent( QwtAbstractScaleDraw::Backbone, false );
    scaleDraw->setTickLength( QwtScaleDiv::MinorTick, 0 );
    scaleDraw->setTickLength( QwtScaleDiv::MediumTick, 3 );
    scaleDraw->setTickLength( QwtScaleDiv::MajorTick, 6 );
    setScaleDraw( scaleDraw );

    setWrapping( false );
    setReadOnly( true );

    setScale(0.0 , 1024.0);
    setOrigin( 135.0 );
    setScaleArc( 0.0, 270.0 );
    setScaleStepSize(100);

    QwtDialSimpleNeedle *needle = new QwtDialSimpleNeedle(
        QwtDialSimpleNeedle::Arrow, true, Qt::red,
        QColor( Qt::gray ).light( 130 ) );
    setNeedle( needle );
}

void SpeedoMeter::setLabel( const QString &label )
{
    d_label = label;
    update();
}

QString SpeedoMeter::label() const
{
    return d_label;
}

void SpeedoMeter::drawScaleContents( QPainter *painter,
    const QPointF &center, double radius ) const
{
    QRectF rect( 0.0, 0.0, 2.0 * radius, 2.0 * radius - 10.0 );

    rect.moveCenter( center );

    const QColor color = palette().color( QPalette::Text );
    painter->setPen( color );

    const int flags = Qt::AlignBottom | Qt::AlignHCenter;
    painter->drawText( rect, flags, d_label );
}

//QPalette SpeedoMeter::colorTheme( const QColor &base ) const
//{
//    QPalette palette;
//    palette.setColor( QPalette::Base, base );//internal color
//    palette.setColor( QPalette::Window, base.dark( 150 ) );//widget background//////////
//    palette.setColor( QPalette::Mid, base.dark( 110 ) );
//    palette.setColor( QPalette::Light, base.light( 170 ) );
//    palette.setColor( QPalette::Dark, base.dark( 170 ) );
//    palette.setColor( QPalette::Text, base.dark( 200 ).light( 800 ) );
//    palette.setColor( QPalette::WindowText, base.dark( 200 ) );

//    return palette;
//}
