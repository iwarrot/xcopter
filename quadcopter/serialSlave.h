#ifndef SerialSlave_H
#define SerialSlave_H

#include <QObject>

#include <QThread>
#include <QTimer>
#include "qextserialport/win_qextserialport.h"

//声明外部类
class ShareManager;

class SerialSlave : public QObject
{
    Q_OBJECT
public:
    explicit SerialSlave(QObject *parent = 0);
    ~SerialSlave();
    //串口指针
    Win_QextSerialPort *serialPort;
    //Polling定时器
    QTimer timer_polling;
    //串口接收线程
    QThread workThread;
    //共享指针
    ShareManager *sharePointer;

    //打开串口
    bool openSerialPort(QString port);

    void closeSerialPort();

    //上位机命令，发给下位机
    void sendCalibrationCMD();

    //接收缓冲区
    QByteArray recBuffer;
private:

    void serialBufferHandle(QByteArray &buf);

    inline void RCHandle(const QByteArray &buf);

    inline void AHRSHandle(const QByteArray &buf);

    inline void GPSHandle(const QByteArray &buf);

    inline void MOTORHandle(const QByteArray &buf);
    
public slots:
    void slot_readSerialData();

    
};

#endif // SerialSlave_H
