#ifndef BUFFERHANDLE_H
#define BUFFERHANDLE_H

#include <QThread>

class ShareManager;

class BufferHandle : public QThread
{
    Q_OBJECT
public:
    explicit BufferHandle(QObject *parent = 0);

    //共享指针
    ShareManager *sharePointer;

    void run();

    void startThread();

private:
    void serialBufferHandle(QByteArray &buf);

    inline void RCHandle(const QByteArray &buf);

    inline void AHRSHandle(const QByteArray &buf);

    inline void GPSHandle(const QByteArray &buf);
    
signals:
    
public slots:
    
};

#endif // BUFFERHANDLE_H
