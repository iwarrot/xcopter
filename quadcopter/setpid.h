#ifndef SETPID_H
#define SETPID_H

#include <QDialog>

namespace Ui {
class SetPID;
}

class SetPID : public QDialog
{
    Q_OBJECT
    
public:
    explicit SetPID(QWidget *parent = 0);
    ~SetPID();
    
private:
    Ui::SetPID *ui;
};

#endif // SETPID_H
