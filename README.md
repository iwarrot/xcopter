# Balalalaaa..
大学毕设是四轴飞行器，于是边学边做的一个Qt地面站。
非常LOW，很多地方回头看做的很辣鸡，但希望能帮助到新新新新新手吧~


# 软件版本
Qt5.5.1 + Qwt6.1.2

# 图传exe使用方法
直接打开MJPG_Streamer.exe，或将其放在和quadcopter.exe同一个目录，在地面站中调用打开

# Baidu地图
将quadcopter\baidumap\map.html放在和quadcopter.exe同一个目录（若过期，则需在Baidu注册一个KEY...)



# Qwt安装/使用方法
Lets go:
1. Download and install QT (MinGw) to: "C:\Qt\Qt5.5.1"
2. Download and extract Qwt qwt-6.1.2 to: "C:\qwt-6.1.2"
3. Add "C:\Qt\Qt5.5.1\5.5\mingw492_32\bin" to your systems path variable (qmake.exe is located here)
4. Add "C:\Qt\Qt5.5.1\Tools\mingw492_32\bin" to your systems path variable (mingw32-make.exe is located here)
5. Open a command line (cmd) and navigate to: "C:\qwt-6.1.2"
6. Type: "qmake" (This command won't prompt any msg so don't worry)
7. Type: "mingw32-make" (Compiles the whole project with examples; this will take a while so be patient)
8. Type: "mingw32-make install" (This installs qwt to the directory set in "C:\qwt-6.1.2\qwtconfig.pri"; CHANGE THE DEFAULT DIRECTORY TO "C:\Qt\Qwt-$$QWT_VERSION-rc3" => "C:\Qt\qwt-6.1.2\")
9. Add a User variable named "QT_PLUGIN_PATH" with the following path "C:\Qt\Qwt-6.1.2\plugins" 
10. Add a User variable named "QMAKEFEATURES" with the following path "C:\Qt\Qwt-6.1.2\features"

=======================================

在Qt项目文件.pro中添加 ：CONFIG += qwt

=======================================